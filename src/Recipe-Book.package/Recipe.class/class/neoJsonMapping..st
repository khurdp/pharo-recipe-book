neoJson
neoJsonMapping: mapper
	mapper for: self do: [ :mapping |
		mapping mapInstVars: #( name description ).
		(mapping mapInstVar: #ingredients) valueSchema: #ings ].
		mapper for: #ings customDo: [ :mapping |
		mapping listOfElementSchema: Ingredient ].