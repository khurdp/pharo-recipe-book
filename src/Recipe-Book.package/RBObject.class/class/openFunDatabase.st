persistence
openFunDatabase
	"comment stating purpose of class-side message"
	"scope: class-variables  &  class-instance-variables"	
	
	database isNil
		ifTrue: [ database := self root databaseNamed: 'FUN' ].
	^database 